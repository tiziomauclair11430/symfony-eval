const input_nom = document.getElementById('produit_nom');
const log_nom = document.getElementById('nom');

input_nom.addEventListener('input', updateValue1);

function updateValue1(e) {
  log_nom.textContent = e.target.value;
}

const input_desc = document.getElementById('produit_description');
const log_desc = document.getElementById('description');

input_desc.addEventListener('input', updateValue2);

function updateValue2(e) {
  log_desc.textContent = e.target.value;
}

const input_prix = document.getElementById('produit_prix');
const log_prix = document.getElementById('prix');

input_prix.addEventListener('input', updateValue3);

function updateValue3(e) {
  log_prix.textContent = e.target.value + '$';
}

let input_image = document.getElementById('produit_image');
let img = document.getElementById('setImage')
upload_image = "";
input_image.setAttribute('value', "nanana")


input_image.addEventListener("change", function(){
  const reader = new FileReader();
  reader.addEventListener("load", ()=> {
    upload_image = reader.result 
    if(img){
    img.setAttribute("src", "")
    }
    document.getElementById('images').style.backgroundImage = `url(${upload_image})`
  })
  reader.readAsDataURL(this.files[0])
})

let input_dispo = document.getElementById('produit_dispo')
let log_dispo = document.querySelector('.ok')

input_dispo.addEventListener('input', updateValue4)

function updateValue4(e) {
  if(e.target.value == 1){
    log_dispo.setAttribute("class", "dispo-edit")
    log_dispo.textContent = "Disponible";
  }else{
    log_dispo.setAttribute("class", "nn-dispo-edit")
    log_dispo.textContent = "Indisponible";
  }
  
}