<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/carte')]
class CarteController extends AbstractController
{
    #[Route('/', name: 'carte_carte',methods:['GET'])]
    public function index(ProduitRepository $produitRepository)
    {
        $produit = array( 
        'produit'=> $produitRepository->findAll(), 
        'showEdit'=> false
    );

        return $this->render('produit/carte.html.twig',$produit);
    }
}