<?php

namespace App\Form;

use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class,[
                'attr' => ['class' => 'form-control mb-2'],
                'label_format' => 'Prenom'
            ])
            ->add('numero', TextType::class,[
                'attr' => ['class' => 'form-control mb-2'],
                'label_format' => 'Numero de téléphone'
            ])
            ->add('date', DateType::class,[
                'attr' => ['class' => 'form-control mb-2'],
                'label_format' => 'Date de la reservation'
            ])
            ->add('moment', ChoiceType::class,[
                'attr' => ['class' => 'form-select'],
                'label_format' => 'Moment de reservation',
                'choices' => [
                    'Midi' => 'Midi',
                    'Soir' => 'Soir',
                ]
            ])
            ->add('nbrPersonne', NumberType::class,[
                'attr' => ['class' => 'form-control mb-2'],
                'label_format' => 'Nombre de personne'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
