<?php

namespace App\Form;

use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom', TextType::class, [
                'attr' => ['class' => 'form-control mb-2 nom_list1 form-control-sm']
            ])
            ->add('description', TextType::class, [
                'attr' => ['class' => 'form-control mb-2 form-control-sm ']
            ])
            ->add('prix', NumberType::class, [
                'attr' => ['class' => 'form-control mb-2 form-control-sm']
            ])
            ->add('dispo', ChoiceType::class, [
                'attr' => ['class' => 'form-select form-control-sm'],
                'choices' => [
                    'Non-Disponible' => 0,
                    'Disponible' => 1,
                ]
            ])
            ->add('image' ,FileType::class,[
                'mapped' => false,
                'attr' => ['class' => 'form-control form-control-sm']
                
            ])
                
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}
